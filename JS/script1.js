const clients1 = ["Гилберт", "Сальваторе", "Пирс", "Соммерс", "Форбс", "Донован", "Беннет"];
const clients2 = ["Пирс", "Зальцман", "Сальваторе", "Майклсон"];

const allClients = new Set([...clients1, ...clients2]);
const allClientsNorepeat = [...allClients];
console.log(allClientsNorepeat);
