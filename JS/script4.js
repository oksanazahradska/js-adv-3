const satoshi2020 = {
    name: 'Nick',
    surname: 'Sabo',
    age: 51,
    country: 'Japan',
    birth: '1979-08-21',
    location: {
      lat: 38.869422, 
      lng: 139.876632
    }
  }
  
  const satoshi2019 = {
    name: 'Dorian',
    surname: 'Nakamoto',
    age: 44,
    hidden: true,
    country: 'USA',
    wallet: '1A1zP1eP5QGefi2DMPTfTL5SLmv7DivfNa',
    browser: 'Chrome'
  }
  
  const satoshi2018 = {
    name: 'Satoshi',
    surname: 'Nakamoto', 
    technology: 'Bitcoin',
    country: 'Japan',
    browser: 'Tor',
    birth: '1975-04-05'
  }



  function fullProfile(...objects){
    const profile = {};
    objects.forEach(satoshi =>{
        for (const[key, value] of Object.entries(satoshi)){
            if(!profile.hasOwnProperty(key)){
            profile[key] = value;
            }
        }
    })
    return profile;
  }

  const profileObject = fullProfile(satoshi2020, satoshi2019, satoshi2018);
  console.log(profileObject);