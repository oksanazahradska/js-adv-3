const user1 = {
    name: "John",
    years: 30
  };
  const names = user1.name;
  const years = user1.years;
  const isAdmin = user1.admin || false;
  
  const el1 = document.createElement('div');
  const el2 = document.createElement('div');
  const el3 = document.createElement('div');
  el1.textContent = names;
  el2.textContent = years;
  el3.textContent = isAdmin;

  const bodyEl = document.body;


  bodyEl.appendChild(el1);
  bodyEl.appendChild(el2);
  bodyEl.appendChild(el3);

